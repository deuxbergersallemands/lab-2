/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wae
 *
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) {
		super(t);
	}
	
	public String toString() {
		//TODO]
		return String.valueOf(this.getValue()) + " F";
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float newVal = (this.getValue() -32) * 5 / 9;   
		Temperature newTemp = new Celsius(newVal);
		return newTemp;
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
	
	public Temperature toKelvin() {
		float newVal = (float) (((this.getValue() -32 ) *5/9) + 273);
		Temperature toReturn = new Kelvin(newVal);
		return toReturn; 
	}
}
