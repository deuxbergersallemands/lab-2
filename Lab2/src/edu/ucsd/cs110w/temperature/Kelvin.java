/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author sil024
 *
 */
public class Kelvin extends Temperature{
	public Kelvin(float t) {
		super(t);
	}
	
	public String toString()
	{
		//TODO: Complete this method
		return String.valueOf(this.getValue()) + " K";
	}
	
	public Temperature toCelsius() {
		float newVal = (this.getValue()) - 273;
		Temperature toReturn = new Kelvin(newVal);
		return toReturn; 
	}
	
	public Temperature toFahrenheit() {
		float newVal = ((this.getValue() - 273) * 9/5) + 32;
		Temperature toReturn = new Fahrenheit(newVal);
		return toReturn; 
	}
	
	public Temperature toKelvin() {
		return this; 
	}
}
