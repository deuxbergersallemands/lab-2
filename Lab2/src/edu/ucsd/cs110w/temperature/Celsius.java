/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wae
 *
 */
public class Celsius extends Temperature{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
			return String.valueOf(this.getValue()) + " C";
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	
	public Temperature toFahrenheit() {
		float newVal = (this.getValue() * 9) / 5 + 32;   
		Temperature newTemp = new Fahrenheit(newVal);
		return newTemp;
	}
	
	public Temperature toKelvin() {
		float newVal = (this.getValue() + 273);
		Temperature newTemp = new Kelvin(newVal);
		return newTemp;  
	}
}
